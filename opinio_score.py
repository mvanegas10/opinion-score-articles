# coding=utf-8
import nltk
import json
import ast
import csv
import re
import math
import pandas as pd
import spacy
from spacy_lefff import LefffLemmatizer, POSTagger
from spacy.language import Language
import fr_core_news_sm
from timeit import default_timer as timer



@Language.factory('french_lemmatizer')
def create_french_lemmatizer(nlp, name):
    return LefffLemmatizer(after_melt=True, default=True)


@Language.factory('melt_tagger')
def create_melt_tagger(nlp, name):
    return POSTagger()


nlp = fr_core_news_sm.load()
nlp.add_pipe('melt_tagger', after='parser')
nlp.add_pipe('french_lemmatizer', after='melt_tagger')


def pos_tags(art):

    tagged_art = nlp(art)
    tags = []
    for a in tagged_art:
        tags.append(a._.melt_tagger)

    return tags


def tokenize(art):
    words = nltk.word_tokenize(art, "french")
    tokens = []
    for word in words:
        lword = word.lower()
        tokens.append(lword)
    return tokens


def remove_citations(article):
    article = re.sub('"(.*?)"', '', article)
    article = re.sub('«(.*?)»', '', article)
    article = re.sub('“(.*?)”', '', article)

    return article


def remove_punct(tokens):
    puncts = [".", ",", "!", "?", "’", "(", ")", "''", "...", "``", "'", "%", "“", ":", "-", ";"]
    lex_tokens = []
    for token in tokens:
        if token not in puncts:
            lex_tokens.append(token)

    return lex_tokens


def count_interrog(tokens):
    cnt = 0
    for i in tokens:
        if i == "?":
            cnt += 1

    return cnt


def count_pointvirg(tokens):
    cnt = 0
    for i in tokens:
        if i == ";":
            cnt += 1

    return cnt


def count_exclam(tokens):
    cnt = 0
    for i in tokens:
        if i == "!":
            cnt += 1

    return cnt


def count_citations(article):
    cnt1 = len(re.findall(r'"(.*?)"', article))
    cnt2 = len(re.findall(r'«(.*?)»', article))
    cnt3 = len(re.findall(r'“(.*?)”', article))
    cnt = cnt1 + cnt2 + cnt3

    return cnt


def count_digits(tokens):
    cnt = 0
    for token in tokens:
        has_digit = False
        for char in token:
            if char.isdigit():
                has_digit = True
        if has_digit:
            if not re.match("ovid", token):
                cnt += 1

    return cnt


def count_pron_1(tokens):
    prons_1 = ["moi", "je", "me", "nous", "mien", "miens", "nos", "notre", "nôtres", "mon", "ma", "mes"]

    cnt = 0
    for token in tokens:
        if token in prons_1:
            cnt += 1

    return cnt


def count_pron_2(tokens):
    prons_2 = ["toi", "tu", "te", "vous", "tien", "tiens", "vos", "votre", "vôtres", "ton", "ta", "tes"]

    cnt = 0
    for token in tokens:
        if token in prons_2:
            cnt += 1

    return cnt


def count_negations(tokens):
    negs = ["ne", "ni", "n", "non", "aucun", "sans"]

    cnt = 0
    for token in tokens:
        if token in negs:
            cnt += 1

    return cnt


def token_length(tokens):
    total = 0
    for token in tokens:
        length = len(token)
        total += length

    return total / len(tokens)


def cttr(tokens):

    types = []
    for token in tokens:
        if token not in types:
            types.append(token)
    cttr = len(types) / math.sqrt(2 * len(tokens))

    return cttr


def count_adv(article):

    pos_art = pos_tags(article)
    cnt = 0
    for art in pos_art:
        if art == 'ADV':
            cnt += 1

    return cnt


def length_tokens(tokens):

    tot_length = 0
    for token in tokens:
        tot_length += len(token)

    return tot_length / len(tokens)


if __name__ == '__main__':

    time1 = timer()

    content_list = {}
    channel_list = {}
    category_list = {}
    views_list = {}
    uniqv_list = {}

    with open('views_10k.csv') as view_file:

        reader2 = csv.reader(view_file, delimiter=',')

        for row in reader2:
            art_id = row[0]
            views = row[1]
            uniq_views = row[2]
            views_list[art_id] = views
            uniqv_list[art_id] = uniq_views

    with open('articles_10k.csv') as csvfile:

        reader = csv.reader(csvfile, delimiter='|')
        #next(reader, None)
        csv.field_size_limit(100000000)

        for row in reader:
            media_id = row[0]
            metadata = row[2]
            metadata = ast.literal_eval(metadata)

            content = metadata["content"]

            content = re.sub('<.*?>', '', content)
            content = re.sub('\xa0', ' ', content)

            content_list[media_id] = content

            channel = metadata["channel"]
            channel_list[media_id] = channel

            category = metadata["category_fullname"]
            category_list[media_id] = category

    # for i, a in content_list.items():
    #     if len(a) > 200:
    #         print(i, a)

    time2 = timer()
    print("chargement des données en secondes : ", time2-time1)

    score_dic = {}
    # k = 0

    for id, article in content_list.items():
        if len(article) > 200:

            opi_score = 0

            cleaned_art = remove_citations(article)
            tokens = tokenize(article)
            cleaned_tokens = tokenize(cleaned_art)
            lex_tokens = remove_punct(cleaned_tokens)

            n_pron1 = count_pron_1(lex_tokens) / len(lex_tokens) * 1000
            opi_score += n_pron1 * 5

            n_pron2 = count_pron_2(lex_tokens) / len(lex_tokens) * 1000
            opi_score += n_pron2 * 6

            n_interr = count_interrog(cleaned_tokens) / len(cleaned_tokens) * 1000
            opi_score += n_interr * 10

            n_negs = count_negations(lex_tokens) / len(lex_tokens) * 100
            opi_score += n_negs * 7

            n_exclam = count_exclam(cleaned_tokens) / len(cleaned_tokens) * 1000
            opi_score += n_exclam * 9

            n_pv = count_pointvirg(cleaned_tokens) / len(cleaned_tokens) * 1000
            opi_score += n_pv * 7

            n_cit = count_citations(article) / len(lex_tokens) * 100
            opi_score -= n_cit * 5

            n_digits = count_digits(cleaned_tokens) / len(cleaned_tokens) * 100
            opi_score -= n_digits * 5

            ttr = cttr(lex_tokens) / 50.0
            opi_score += ttr * 3

            # print(id, n_pron1, n_pron2, n_interr, n_exclam, n_pv, n_negs, n_digits, n_cit)

            score_dic[id] = opi_score
            # k+=1
            # if k == 15:
            #     break

    time3 = timer()
    print("étape 2 : ", time3-time1)

    l = sorted((value, key) for (key, value) in score_dic.items())

    # l.reverse()

    time4 = timer()
    print("tri de liste : ", time4-time1)

    opi_list = []
    societe_list = []
    opi_views = 0.0
    soc_views = 0.0

    for i in l:
        if channel_list[i[1]] == "RTBFINFO":
            if category_list[i[1]].lower() == "opinions" or category_list[i[1]].lower() == "chroniques" or category_list[i[1]].lower() == "les grenades":
                opi_list.append(i)
                # print(i, channel_list[i[1]])
                # print(content_list[i[1]])
                # print(category_list[i[1]])
                # print(views_list[i[1]])
                # print(uniqv_list[i[1]])
            if category_list[i[1]].lower() == "societe" or category_list[i[1]].lower() == "belgique" or category_list[i[1]].lower() == "regions":
                societe_list.append(i)
                print(i, channel_list[i[1]])
                print(content_list[i[1]])
                print(category_list[i[1]])
                print(views_list[i[1]])
                print(uniqv_list[i[1]])



    total_opi = 0

    for j in opi_list:
        # print(j, channel_list[j[1]])
        # print(content_list[j[1]])
        # print(category_list[j[1]])
        total_opi += j[0]
        opi_views += float(views_list[j[1]])

    total_soc = 0

    for m in societe_list:
        # print(m, channel_list[m[1]])
        # print(content_list[m[1]])
        # print(category_list[m[1]])
        total_soc += m[0]
        soc_views += float(views_list[m[1]])


    print("moyenne score opi : ", total_opi/len(opi_list))
    print("moyenne score soc : ", total_soc/len(societe_list))

    print("moyenne vues opi : ", opi_views/len(opi_list))
    print("moyenne vues soc : ", soc_views/len(societe_list))

    time5 = timer()
    print("fin : ", time5-time1)