# opinion-score-articles

This project contains the result of one of Louis Escouflaire's one-month internship project at RTBF's Data Management team. The main objective was identifying opinion in RTBF's articles via lexical analysis (punctuation and grammar).